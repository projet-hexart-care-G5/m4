//
// Created by matth on 13/11/2018.
//

#ifndef M4_ACTIONS_H
#define M4_ACTIONS_H

#include "donnees.h"

void copy(Data *tab, Data *tab2, int size);

void reverse_tab(Data *tab, int size);

void quick_sort_croissant_pulse(int nombre_de_ligne, Data *tableau_data);

void quick_sort_croissant_time(int nombre_de_ligne, Data *tableau_data);

int search_by_time(Data *info, int lines, int time);

double average_pulse(Data *info, Data *cp, int line);

#endif //M4_ACTIONS_H
