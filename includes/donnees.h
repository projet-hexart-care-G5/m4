//
// Created by matth on 13/11/2018.
//

#ifndef M4_DONNEES_H
#define M4_DONNEES_H

#define PATH "data.csv"

typedef struct Data {
    int time, pouls;
}Data;

int count_line();

void init_data(Data *info);

void print_data(Data *info, int lines);

#endif //M4_DONNEES_H
