//
// Created by matth on 13/11/2018.
//

#include <stdio.h>
#include "../includes/actions.h"
#include "../includes/donnees.h"
#include "../includes/menu.h"

/*
 *  Fonction de copie de tableaux
 */
void copy(Data *tab, Data *tab2, int size) {
    for (int i = 0; i < size; i++) {
        tab2[i].time = tab[i].time;
        tab2[i].pouls = tab[i].pouls;
    }
}

/*
 *  Fonction qui permet d'inverser le contenu d'un tableau
 */
void reverse_tab(Data *tab, int size) {
    Data ntab[size];
    copy(tab, ntab, size);
    int n = 0;
    for (int i = size; i > 0; i--) {
        tab[n].time = ntab[i - 1].time;
        tab[n].pouls = ntab[i - 1].pouls;
        n++;
    }
}

/*
 *  Fonction qui classe un tableau par pouls dans l'ordre croissant
 */
void quick_sort_croissant_pulse(int nombre_de_ligne, Data *tableau_data) {
    int limite;
    int courant;
    Data pivot;
    Data tmp;

    if (nombre_de_ligne < 2)
        return;

    // On prend comme pivot l element le plus a droite
    pivot = tableau_data[nombre_de_ligne - 1];
    limite = 0;
    courant = 0;
    while (courant < nombre_de_ligne) {
        if (tableau_data[courant].pouls <= pivot.pouls) {
            if (limite != courant) {
                tmp = tableau_data[courant];
                tableau_data[courant] = tableau_data[limite];
                tableau_data[limite] = tmp;
            }
            limite++;
        }
        courant++;
    }
    quick_sort_croissant_pulse(limite - 1, tableau_data);
    quick_sort_croissant_pulse(nombre_de_ligne - limite + 1, tableau_data + limite - 1);
}

/*
 *  Fonction qui classe un tableau par temps dans l'ordre croissant
 */
void quick_sort_croissant_time(int nombre_de_ligne, Data *tableau_data) {
    int limite;
    int courant;
    Data pivot;
    Data tmp;

    if (nombre_de_ligne < 2)
        return;

    // On prend comme pivot l element le plus a droite
    pivot = tableau_data[nombre_de_ligne - 1];
    limite = 0;
    courant = 0;
    while (courant < nombre_de_ligne) {
        if (tableau_data[courant].time <= pivot.time) {
            if (limite != courant) {
                tmp = tableau_data[courant];
                tableau_data[courant] = tableau_data[limite];
                tableau_data[limite] = tmp;
            }
            limite++;
        }
        courant++;
    }
    quick_sort_croissant_time(limite - 1, tableau_data);
    quick_sort_croissant_time(nombre_de_ligne - limite + 1, tableau_data + limite - 1);
}

/*
 *  Fonction qui recherche un pouls avec le temps
 */
int search_by_time(Data *info, int lines, int time) {

    for (int i = 0; i < lines; ++i) {
        if (info[i].time == time)
            return info[i].pouls;
    }
    return 0;
}


double average_pulse(Data *info, Data *cp, int lines) {

    int min = 0;
    int max = 0;

    printf("Veuillez renter le MIN :\n");
    scanf("%d", &min);
    printf("Veuillez renter le MAX :\n");
    scanf("%d", &max);

    double moy = 0;
    int nb = 0;

    for (int i = 0; i < lines; i++) {
        if (cp[i].time >= min && cp[i].time <= max) {
            moy += cp[i].pouls;
            nb++;
        }
    }
    return moy / nb;
}

