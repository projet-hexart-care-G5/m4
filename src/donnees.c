//
// Created by matth on 13/11/2018.
//
#include <stdio.h>
#include <mem.h>
#include <stdlib.h>
#include "../includes/donnees.h"

/*
 *   Fonction qui compte le nombre de ligne
 */
int count_line() {

    FILE *file = fopen(PATH, "r");
    char line[256];

    int i = 0;

    while (fgets(line, sizeof(line), file)) {
        i++;
    }
    return i;
}

/*
 *  Fonction qui initialise le programme
 */
void init_data(Data *info) {

    FILE *file = fopen(PATH, "r");
    char line[256];
    int lines = 0;

    while (fgets(line, sizeof(line), file)) {

        char *s;

        s = strtok(line, ";");
        info[lines].time = atoi(s);

        s = strtok(NULL, ";");
        info[lines].pouls = atoi(s);

        lines++;
    }

    fclose(file);
}

/*
 * Fonction qui affiche le contenu dans l'ordre
 */
void print_data(Data *info, int lines) {

    for (int i = 0; i < lines; ++i) {
        printf("Temps : %d | Pouls : %d\n", info[i].time, info[i].pouls);
    }
}