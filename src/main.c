#include <stdio.h>
#include <mem.h>
#include <stdlib.h>
#include "../includes/donnees.h"
#include "../includes/menu.h"

int lines = 0;

int main() {

    lines = count_line();   // Nombre de lignes
    Data info[lines];       // Declaration du tableau de structure

    init_data(info);        // Initialisation des données du CSV
    show_menu(info, lines); // Affichage du menu

    return 0;
}