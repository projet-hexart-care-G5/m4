//
// Created by matth on 13/11/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include "../includes/menu.h"
#include "../includes/donnees.h"
#include "../includes/actions.h"


/*
 *  Fonction qui permet d'afficher le menu de selection
 */
void show_menu(Data *info, int lines) {

    for (;;) {
        printf(" 1 -  Afficher les donnees dans l'ordre du CSV.\n");
        printf(" 2 -  Afficher les donnees dans l'odre croissant (temps)\n");
        printf(" 3 -  Afficher les donnees dans l'odre croissant (pouls)\n");
        printf(" 4 -  Afficher les donnees dans l'odre decroissant (temps)\n");
        printf(" 5 -  Afficher les donnees dans l'odre decroissant (pouls)\n");
        printf(" 6 -  Rechercher et afficher donnee pour un tps particulier\n");
        printf(" 7 -  Afficher la moyenne de pouls dans une plage de tps donnee\n");
        printf(" 8 -  Afficher le nombre de ligne actuellement en memoire\n");
        printf(" 9 -  Rechercher et afficher les min/max de pouls\n");
        printf(" 0 -  Quitter l'application\n");
        int response;

        scanf("%d", &response);

        int time = -1;
        Data cp[lines];

        switch (response) {
            case 0:
                exit(1);
            case 1:
                print_data(info, lines);
                break;
            case 2:
                copy(info, cp, lines);
                quick_sort_croissant_time(lines, cp);
                print_data(cp, lines);
                break;
            case 3:
                copy(info, cp, lines);
                quick_sort_croissant_pulse(lines, cp);
                print_data(cp, lines);
                break;
            case 4:
                copy(info, cp, lines);
                quick_sort_croissant_time(lines, cp);
                reverse_tab(cp, lines);
                print_data(cp, lines);
                break;
            case 5:
                copy(info, cp, lines);
                quick_sort_croissant_pulse(lines, cp);
                reverse_tab(cp, lines);
                print_data(cp, lines);
                break;
            case 6:
                while (time == -1) {
                    printf("Veuillez renter le temps :\n");
                    scanf("%d", &time);
                }
                print_line();
                printf("Le pouls correspondant a %dms est : %d BPM\n", time, search_by_time(info, lines, time));
                print_line();
                break;

            case 8:
                print_line();
                printf("Nombre de lignes : %d\n", lines);
                print_line();
                break;

            case 7:
                copy(info, cp, lines);
                quick_sort_croissant_time(lines, cp);
                double moy = average_pulse(info, cp, lines);
                print_line();
                printf("Moyenne de pouls : %f\n", moy);
                print_line();
                break;

            case 9:
                copy(info, cp, lines);
                quick_sort_croissant_pulse(lines, cp);
                print_line();
                printf("MIN : %d\n", cp[0].pouls);
                printf("MAX : %d\n", cp[lines - 1].pouls);
                print_line();
                break;

            default:
                break;
        }
    }
}

void  print_line() {
    printf("--------------------\n");
}